from pymongo import MongoClient
from bson.objectid import ObjectId
import os

class Database():
    def __init__(self):
        os.makedirs("C:/Database Proyecto", exist_ok=True)
        Mongo_URI = 'mongodb://localhost'
        client = MongoClient(Mongo_URI)
        db = client['Database_proyecto']
        self.user = db["Usuarios"]
        self.pacientes = db['Pacientes']
        self.personal_asistencia = db['Personal_Asistencial']
        self.examenes = db['Examenes']

    def agregar_usuario(self, datos):
        """
        {
            "usuario": "airon",
            "password": "contraseña",
            "roll": "estudiante",
            "correo": "algo@.."
        }
        """
        return self.user.insert_one(datos).inserted_id

    def agregar_paciente(self, datos):
        """
        {
            "id_usuario": "123456789",
            "nombres": "airon",
            "apellidos": "ruda",
            "correo": "algo@..",
            "telefonos": [],
            "documento": 123456789,
            "fechaNacido": "12/34/5678",
            "genero": "hombre",
            "examenes":[]
        }
        """
        self.pacientes.insert_one(datos)

    def agregar_personal_asistencial(self, datos):
        """
        {
            "id_usuario": "123456789",
            "nombres": "airon",
            "apellidos": "ruda",
            "correo": "algo@..",
            "telefonos": [],
            "documento": "123456789",
            "fechaNacido": "12/34/5678",
            "genero": "hombre",
            "especialidad":[],
            "registro": "1234"
        }
        """
        search = self.personal_asistencia.find({'documento': datos['documento']})
        find = False
        for results in search:
            find = True
        if find == False:
            self.personal_asistencia.insert_one(datos)
        return True


    def agregar_examenes(self, datos):
        """
        {
            "id_personal": "123456789",
            "FechaRealizacion": "12/34/456",
            "modalidad": "Examen de sangre",
            "path": "D\\algo\\algomás",
            "numeroOrden": 1234,
            "dirSede": "calle no que mondá",
            "telefonoSede": 123456,
        }
        """
        return self.examenes.insert_one(datos).inserted_id

    def buscar_usuario(self, datos):
        search = self.user.find({'usuario': datos})
        find = False
        info = {}
        for results in search:
            find = True
            info = results
        return find, info

    def buscar_paciente(self, datos, tipo):
        if tipo == 1: search = self.pacientes.find({'id_usuario': datos})
        else: search = self.pacientes.find({'documento': datos})
        find = False
        info = {}
        for results in search:
            find = True
            info = results
        return find, info

    def buscar_personal_asistencial(self, datos, tipo):
        if tipo == 1: search = self.personal_asistencia.find({'id_usuario': datos})
        else: search = self.personal_asistencia.find({'documento': datos})
        find = False
        info = {}
        for results in search:
            find = True
            info = results
        return find, info


    def buscar_examenes(self, datos):
        
        search = self.examenes.find({'_id': datos})
        find = False
        info = {} 
        for results in search:
            find = True
            info = results
        return find, info

    def actualizar_paciente(self, doc, id_examen):
        search = self.pacientes.find({'documento':doc})
        find = False

        for results in search:
            find = True
            results['examenes'].append(id_examen)
            self.pacientes.delete_one({'documento':doc})
            self.pacientes.insert_one(results)
    
    def actualizar_perfil_paciente(self, doc, datos):
        search = self.pacientes.find({'documento':doc})
        for results in search:
            find = True
            datos["id_usuario"] = results["id_usuario"]
            datos["examenes"] = results["examenes"]
            self.pacientes.delete_one({'documento':doc})
            self.pacientes.insert_one(datos)

    def actualizar_usuario_paciente(self, doc, datos):
        search = self.user.find({'usuario':doc})
        for results in search:
            find = True
            datos["_id"] = results["_id"]
            self.user.delete_one({'usuario':doc})
            self.user.insert_one(datos)
    
    def get_examen_archivo(self, busqueda):
        results = self.examenes.find(busqueda)
        for result in results:
            return True, result
        return False, {}
    
    def delete_examen(self, ide, doc):
        self.examenes.delete_one({"_id": ObjectId(ide)})
        result = self.pacientes.find({"documento": doc})
        examenes2 = []
        for resul in result:
            examenes = resul["examenes"]
            for i in examenes:
                if i != ObjectId(ide):
                    examenes2.append(i)
        self.pacientes.update_one({"documento":doc},{'$set': {'examenes': examenes2}})
        

