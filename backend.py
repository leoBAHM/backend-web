from flask import Flask
from flask import render_template
from flask import redirect
from flask import url_for
from flask import request
from flask import session
from flask import send_file
from flask import send_from_directory
from flask import current_app

from werkzeug.security import generate_password_hash as genph
from werkzeug.security import check_password_hash as checkph
from werkzeug.utils import secure_filename
import smtplib 

import os
from data_base_pf import Database
from datetime import datetime

#Config of project
app = Flask(__name__)
app.secret_key = "mysecretkey"
db = Database()
app.config["UPLOAD_FILE"] = r"C:\Database Proyecto"

#%%Run Login and autentication
@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        user = request.form["user"]
        password = request.form["password"]
        bandera = acceso(user, password)
        if bandera:
            if session["roll"] == "Paciente": 
                if session["perfil"] == "No":
                    return render_template("mensaje.html", doc = session["documento"])
                else: return redirect(url_for("perfil_paciente"))
            elif session["roll"] == "Asistencial": return redirect(url_for("perfil_asistencial"))
    return render_template("login.html")

def acceso(user, password):
    bandera, resultados = db.buscar_usuario(user)
    if bandera:
        if user == resultados["usuario"]:
            if password == resultados["password"]:
                session["usuario"] = user
                session["roll"] = resultados["roll"]
                if session["roll"] == "Paciente": 
                    bandera, results = db.buscar_paciente(resultados["_id"],1)
                    session["perfil"] = results["perfil"]
                elif session["roll"] == "Asistencial": bandera, results = db.buscar_personal_asistencial(resultados["_id"],1)
                session["documento"] = results["documento"]
                return True
    return False    

#%%Patients Menu
@app.route("/paciente/perfil")
def perfil_paciente():
    if session["usuario"] == "": return redirect(url_for("index"))
    bandera, results = db.buscar_paciente(session["documento"], 2)
    return render_template("patient_profile.html", info=results)

#%%Pacientes Registros
@app.route("/paciente/registros")
def ver_registros():
    if session["usuario"] == "": return redirect(url_for("index"))
    bandera, results = db.buscar_paciente(session["documento"], 2)
    examenes = get_examenes_archivo(results["examenes"], "Archivo")
    return render_template("patient_register.html", examenes = examenes)   

#%%Pacientes señales
@app.route("/paciente/señales")
def ver_signals():
    if session["usuario"] == "": return redirect(url_for("index"))
    bandera, results = db.buscar_paciente(session["documento"], 2)
    examenes = get_examenes_archivo(results["examenes"], "Señales")
    return render_template("patient_signal.html", examenes=examenes)

#%%Pacientes Imágenes
@app.route("/paciente/imagenes")
def ver_imagenes():
    if session["usuario"] == "": return redirect(url_for("index"))
    bandera, results = db.buscar_paciente(session["documento"], 2)
    examenes = get_examenes_archivo(results["examenes"], "Imagen")
    return render_template("patient_images.html", examenes=examenes)

def get_examenes_archivo(examenes, modalidad):
    resultados = []
    for examen in examenes:
        bandera, results = db.get_examen_archivo({"_id": examen, "modalida": modalidad})
        if bandera == True: resultados.append(results)
    return resultados

#%%Actualizar Perfil
@app.route("/actualizar", methods=["GET", "POST"])
def actualizar():
    if session["usuario"] == "": return redirect(url_for("index"))
    if request.method == "POST":
        if request.form["password"] == request.form["password2"]:
            datos = {
                "documento": session["documento"],
                "nombres": request.form["nombres"],
                "apellidos": request.form["apellidos"],
                "genero": request.form["genero"],
                "edad": request.form["edad"],
                "telefono": request.form["telefono"],
                "correo": request.form["correo"],
                "eps": request.form["eps"],
                "perfil": "Si"
            }
            db.actualizar_perfil_paciente(session["documento"], datos)
            user = {
                "usuario": request.form["user"],
                "password": request.form["password"],
                "roll": "Paciente",
                "correo": request.form["correo"]
            }
            db.actualizar_usuario_paciente(session["documento"], user)
        return redirect(url_for("perfil_paciente"))
    return render_template("actualizar_info.html")

#%%Perfil asistencia
@app.route("/asistencial/perfil")
def perfil_asistencial():
    if session["usuario"] == "": return redirect(url_for("index"))
    banderas, results = db.buscar_personal_asistencial(session["documento"],2)
    return render_template("care_profile.html", info=results)

#%%Load 
@app.route("/asistencial/guardar", methods=["GET", "POST"])
def cargar_asistencial():
    if session["usuario"] == "": return redirect(url_for("index"))
    if request.method == "POST":
        doc = request.form["document"]
        f = request.files["filename"]
        modalidad = request.form["modality"]
        tipo = request.form["type"]
        n_orden = request.form["order_number"]
        direccion = request.form["address"]
        telefono = request.form["telephone"]
        upload_path = os.path.join(app.config["UPLOAD_FILE"], doc, modalidad)
        os.makedirs(upload_path, exist_ok=True)
        filename = os.path.join(upload_path, secure_filename(f.filename))
        f.save(filename)
        dia = datetime.now()
        datos_examen = {
            "doc_personal": session["documento"],
            "fechaRealizacion": dia.strftime("%d/%m/%y"),
            "modalida": modalidad,
            "tipo": tipo,
            "numeroOrden": n_orden,
            "dirSede": direccion,
            "telonoSede": telefono,
            "path": filename
        }
        resultado = db.agregar_examenes(datos_examen)
        asociar_examen(doc, resultado)

        return redirect(url_for("cargar_asistencial"))
    return render_template("care_load.html", info={})

#%%Asistencial buscar
@app.route("/asistencial/consultar", methods=["GET", "POST"])
@app.route("/asistencial/consultar/<string:ide>/<string:doc>")
def buscar_asistencial(ide=None, doc=None):
    if session["usuario"] == "": return redirect(url_for("index"))
    if ide:
        db.delete_examen(ide, doc)
        bandera, results = db.buscar_paciente(doc, 2)
        if bandera: 
            examenes = get_examenes(results["examenes"])
            return redirect(url_for("buscar_asistencial", ide=None, doc=None))
        else: return redirect(url_for("buscar_asistencial", ide=None, doc=None))
    else:
        if request.method == "POST":
            doc = request.form["document"]
            bandera, results = db.buscar_paciente(doc, 2)
            if bandera: 
                examenes = get_examenes(results["examenes"])
                return render_template("care_search.html", info = results, examenes = examenes)
            else: return redirect(url_for("buscar_asistencial"))
        return render_template("care_search.html", info= {}, examenes = [])

def get_examenes(examenes):
    resultados = []
    for examen in examenes:
        bandera, results = db.buscar_examenes(examen)
        resultados.append(results)
    return resultados

@app.route("/download/<path:filename>", methods=['GET', 'POST'])
def download(filename):
    return send_file(filename)

def asociar_examen(doc, id_examen):
    bandera, results = db.buscar_paciente(doc, 2)
    if bandera == True:
        db.actualizar_paciente(doc, id_examen)
        enviar_correo(results.get("correo", ""))
    else:
        user = {
            "usuario": str(doc),
            "password": str(doc),
            "roll": "Paciente",
            "correo": ""
        }
        results = db.agregar_usuario(user)
        datos = {
            "documento": doc,
            "id_usuario": results,
            "perfil": "No",
            "examenes": [id_examen]
        }
        db.agregar_paciente(datos)

    return render_template("care_load.html", info={})

def enviar_correo(correo):
    if correo != "":
        mensaje = "Subject: Examen nuevo\n\nSe ha subido un nuevo archivo al sistema."
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.starttls()
        server.login("fba.IS.PAC@gmail.com","ingenieria4")
        if correo != "": server.sendmail("fba.IS.PAC@gmail.com", correo, mensaje)
        server.quit()
    
#%%Cerrar Sesion
@app.route("/cerrar_sesion")
def cerrar_sesion():
    return redirect(url_for("index"))

if __name__ == "__main__":
    app.run(port=2000)